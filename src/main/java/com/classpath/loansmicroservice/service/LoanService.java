package com.classpath.loansmicroservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.classpath.loansmicroservice.model.Account;
import com.classpath.loansmicroservice.model.Loan;
import com.classpath.loansmicroservice.repository.AccountRepository;
import com.classpath.loansmicroservice.repository.LoansRepository;
@Service
public class LoanService {
//	
//	@Autowired
//	private AccountRepository acrepo;
	
	@Autowired
	private LoansRepository loanRepo;
	


public Loan applyLoan(Loan loan) {
	
	return loanRepo.save(loan);	
	
}

public List<Loan> loanDetails(){
	return loanRepo.findAll();
	
}

public Loan getLoanDetailsById(Long id) {
	
	return loanRepo.findById(id).orElse(null);
}




}
