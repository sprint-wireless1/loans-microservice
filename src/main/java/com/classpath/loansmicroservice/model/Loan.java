package com.classpath.loansmicroservice.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "loan")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Loan {
	
	@Id 
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column (name="loan_id")
	private Long loan_id;
	
	
	@JsonProperty(value = "loan_amount", required = true)
	private BigDecimal loan_amount;
	
	@JsonProperty(value = "balance_amount", required = true)
	private BigDecimal balance_amount;
	
	@JsonProperty(value = "balance_tenure", required = true)
	private BigDecimal balance_tenure;
	
	@JsonProperty(value = "tenure", required = true)
	private BigDecimal tenure;
	
	@JsonProperty(value = "status", required = true)
	private String status;
	
	
	@JsonProperty(value = "customer", required = true)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name= "customer_id")
	private Customer customer;
	
	

}
