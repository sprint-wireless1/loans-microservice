package com.classpath.loansmicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.loansmicroservice.model.Loan;
import com.classpath.loansmicroservice.service.LoanService;

@CrossOrigin 
@RestController
@RequestMapping("/api")
public class LoansController {




@Autowired 
@Qualifier("loanService") 
private LoanService loanService;

//   @PostMapping("/loans")
//   public ResponseEntity<Object> applyLoan(@RequestBody LoanRequest lr ){
//	return loanService.loanEntry;
//	   
//   }

@PostMapping("/applyloans")
public Loan applyLoan(@RequestBody Loan loan) {
	return loanService.applyLoan(loan);
	
}

@GetMapping("/loanlist")
public List<Loan> findAllLoan( ) {
	return loanService.loanDetails();
	
}

@GetMapping("/loan/{id}")
public Loan findLoanById(@PathVariable Long id ) {
	return loanService.getLoanDetailsById(id);
	
}



}
