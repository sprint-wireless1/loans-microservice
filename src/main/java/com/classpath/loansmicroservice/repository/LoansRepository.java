package com.classpath.loansmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.classpath.loansmicroservice.model.Loan;

@Repository
public interface LoansRepository extends JpaRepository<Loan, Long> {

}
